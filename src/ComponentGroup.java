import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

public class ComponentGroup {
	private JCheckBox cb;
	private JTextField classField;
	private JButton editButton;
	private JButton deleteButton;
	private String className;
	private String rosterPath;
	
	public ComponentGroup(JCheckBox cb, JTextField classField, JButton editButton, JButton deleteButton) {
		this.cb = cb;
		this.classField = classField;
		this.classField.setEditable(false);
		this.editButton = editButton;
		this.deleteButton = deleteButton;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getRosterPath() {
		return rosterPath;
	}

	public void setRosterPath(String rosterPath) {
		this.rosterPath = rosterPath;
	}
	
	public JCheckBox getCheckBox() {
		return cb;
	}

	public JButton getEditButton() {
		return editButton;
	}

	public JButton getDeleteButton() {
		return deleteButton;
	}

	public JTextField getClassField() {
		return classField;
	}
}
