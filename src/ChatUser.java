public class ChatUser {

	private String name; // First and last combined into one String
	private String firstEntry; // Time stamp this student first made a comment in Chat
	private String lastEntry; // Time stamp this student last made a comment in Chat
	
	public ChatUser(String name, String firstEntry, String lastEntry) {
		super();
		this.name = name;
		this.firstEntry = firstEntry;
		this.lastEntry = lastEntry;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstEntry() {
		return firstEntry;
	}
	public void setFirstEntry(String firstEntry) {
		this.firstEntry = firstEntry;
	}
	public String getLastEntry() {
		return lastEntry;
	}
	public void setLastEntry(String lastEntry) {
		this.lastEntry = lastEntry;
	}
}
