import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

public class ZoomAttendance {

	private static final String PREF_FILE = "ZoomAttendance.prefs";
	private static String LAST_CHAT_PATH_KEY = "lastChatPath";
	private static String CLASSES_KEY = "classes";
	private static String CLASS_NAME_KEY = "className";
	private static String ROSTER_PATH_KEY = "rosterPath";
	private static String LAST_ROSTER_PATH_KEY = "lastRosterPath";

	public static void main(String[] args) throws JsonSyntaxException, FileNotFoundException, IOException {

		/*
		 * To do:
		 *  - Test for errors and special cases, missing files, etc
		 */
		
		/*
		 * 1. Read Roster names into a List of Strings
		 * 2. Initialize a new List of ChatUser
		 * 3. For each line of the Zoom Chat
		 * 		Read first token into a time String and trim it
		 * 		Read rest of line and trim it
		 * 		We assume that the line is now one of two formats:
		 * 		 - A message to "Everyone" has the format "From <name> : <message>"
		 * 		 - A private message has the format "From <name>  to  <name>(Privately) : <message>
		 * 		
		 * 		If the line contains "  to  " before ":" then it is format 2
		 * 		else format 1
		 * 
		 * 		Format 1
		 * 			Extract <name> and <message>
		 * 
		 * 		Format 2
		 * 			Extract <name> and skip everything up to " : ".
		 * 			Extract <message>
		 * 
		 * 		Search ChatUser for <name>
		 * 		If none exists, build new ChatUser object from the data and put in list
		 * 		Otherwise update the last entry time
		 * 
		 * 4. Finally, loop through the original list of roster names
		 * 		If current name is not found in ChatUser, this student was absent
		 * 			so print out absent information
		 *      Otherwise print out the matching ChatUser object and remove this
		 *      	record from the ChatUser list
		 *      
		 *      Anybody left in the ChatUser list does not match with a roster name.
		 *      Print this list out
		 */

		
		/* Set up GUI
		 * -----------------------------------------------
		 * CBox	Class	Roster Location		Edit	Delete
		 * CBox	Class	Roster Location		Edit	Delete
		 * CBox	Class	Roster Location		Edit	Delete
		 * Add New Class
		 * 
		 * 				Take Attendance
		 */

		// Turns debug printing on/off
		boolean debugState = false;
		
		JButton attendButton = new JButton("Take Attendance");
		JButton addButton = new JButton("Add");
		JPanel blank = new JPanel();
		ButtonGroup bg = new ButtonGroup();
		
		JFrame win = new JFrame("Zoom Chat Attendance");
		win.setSize(800, 400);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		win.setLocation(dim.width/2-win.getSize().width/2, dim.height/2-win.getSize().height/2);
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		panel.setLayout(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
	    c.insets = new Insets(2, 5, 2, 5);

	    // Keeps track of each row of components so rows can be added/deleted
	    ArrayList<ComponentGroup> componentList = new ArrayList<ComponentGroup>();
	    
	    JLabel l0 = new JLabel("Choose");
	    JLabel l1 = new JLabel("Class");
	    JLabel l3 = new JLabel("Edit");
	    JLabel l4 = new JLabel("Delete");
		c.gridx = 0; c.gridy = 0; panel.add(l0, c);
		c.gridx = 1; c.gridy = 0; panel.add(l1, c);
		c.gridx = 2; c.gridy = 0; panel.add(l3, c);
		c.gridx = 3; c.gridy = 0; panel.add(l4, c);
		
		// Add components based on prefs file
		JsonObject prefs = getPrefs();
		
		if (!prefs.has(CLASSES_KEY)) {
			prefs.add(CLASSES_KEY, new JsonArray());
		}
		
		JsonArray classes = prefs.get(CLASSES_KEY).getAsJsonArray();
		
		
		
		//HashMap<String, String> prefsMap = new HashMap<String, String>();
		//ArrayList<String> prefsKeys = new ArrayList<String>();
		
		for (JsonElement elem : classes) {
			//String line = prefsReader.nextLine();
			JsonObject classObj = elem.getAsJsonObject();
			String className = classObj.get(CLASS_NAME_KEY).getAsString();
			String rosterPath = classObj.get(ROSTER_PATH_KEY).getAsString();
			
			JCheckBox cb = new JCheckBox();
			cb.setSelected(true);
			JTextField classField = new JTextField(className, 12);
			classField.setEditable(false);
			
			JButton editButton = new JButton("Edit");
			editButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = componentIndex(componentList, e.getSource());
					String newClassName = JOptionPane.showInputDialog("Rename this class");
					componentList.get(index).getClassField().setText(newClassName);

					JFileChooser rosterChooser = new JFileChooser();
					rosterChooser.setDialogTitle("Choose a roster");
					if (prefs.has(LAST_ROSTER_PATH_KEY)) rosterChooser.setCurrentDirectory(new File(prefs.get(LAST_ROSTER_PATH_KEY).getAsString()));
					rosterChooser.showOpenDialog(null);
					File rosterPath = rosterChooser.getSelectedFile();
					if (rosterPath == null)
						return;
					String cName = componentList.get(index).getClassName();
					
					classObj.addProperty(CLASS_NAME_KEY, newClassName);
					classObj.addProperty(ROSTER_PATH_KEY, rosterPath.getAbsolutePath());
					prefs.addProperty(LAST_ROSTER_PATH_KEY, rosterPath.getParent());
					
					componentList.get(index).setRosterPath(rosterPath.getAbsolutePath());
					
					FileUtil.writeString(PREF_FILE, prefs.toString());					
				}
			});
			
			JButton deleteButton = new JButton("Delete");
			deleteButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = componentIndex(componentList, e.getSource());
					panel.remove(componentList.get(index).getCheckBox());
					panel.remove(componentList.get(index).getClassField());
					panel.remove(componentList.get(index).getEditButton());
					panel.remove(componentList.get(index).getDeleteButton());
					classes.remove(classObj);
					componentList.remove(index);
					win.validate();
					win.pack();
					win.repaint();

					FileUtil.writeString(PREF_FILE, prefs.toString());					
				}
			});

			
			ComponentGroup cg = new ComponentGroup(cb, classField, editButton, deleteButton);
			cg.setClassName(className);
			cg.setRosterPath(rosterPath);
			componentList.add(cg);
			c.gridx = 0; c.gridy = componentList.size(); panel.add(cb, c);
			c.gridx = 1; c.gridy = componentList.size(); panel.add(classField, c);
			c.gridx = 2; c.gridy = componentList.size(); panel.add(editButton, c);
			c.gridx = 3; c.gridy = componentList.size(); panel.add(deleteButton, c);		
		}
		
		// Add button
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String className = JOptionPane.showInputDialog("Name this class");
				JFileChooser rosterChooser = new JFileChooser();
				if (prefs.has(LAST_ROSTER_PATH_KEY)) rosterChooser.setCurrentDirectory(new File(prefs.get(LAST_ROSTER_PATH_KEY).getAsString()));
				rosterChooser.setDialogTitle("Choose a roster");
				rosterChooser.showOpenDialog(null);
				File rosterPath = rosterChooser.getSelectedFile();
				if (rosterPath == null)
					return;

				JCheckBox cb = new JCheckBox();
				bg.add(cb);
				cb.setSelected(true);
				JTextField classField = new JTextField(className, 12);
				classField.setEditable(false);
				JButton editButton = new JButton("Edit");
				JButton deleteButton = new JButton("Delete");
				ComponentGroup cg = new ComponentGroup(cb, classField, editButton, deleteButton);
				cg.setClassName(className);
				cg.setRosterPath(rosterPath.getAbsolutePath());
				// Remove last two buttons
				panel.remove(addButton);
				panel.remove(attendButton);
				// Add new row
				componentList.add(cg);
				c.gridwidth = 1;
				c.gridx = 0; c.gridy = componentList.size(); panel.add(cb, c);
				c.gridx = 1; c.gridy = componentList.size(); panel.add(classField, c);
				c.gridx = 2; c.gridy = componentList.size(); panel.add(editButton, c);
				c.gridx = 3; c.gridy = componentList.size(); panel.add(deleteButton, c);		
				// Add last two buttons (and spacer between them)
				c.gridx = 2; c.gridy = componentList.size() + 1;
				panel.add(addButton, c);				
				c.gridx = 0; c.gridy = componentList.size() + 2;
				c.gridwidth = 5;
				panel.add(blank, c);
				c.gridx = 0; c.gridy = componentList.size() + 3;
				c.gridwidth = 5;
				panel.add(attendButton, c);
				win.validate();
				win.pack();
				win.repaint();
				
				JsonObject classObject = new JsonObject();
				classObject.addProperty(CLASS_NAME_KEY, className);
				classObject.addProperty(ROSTER_PATH_KEY, rosterPath.getAbsolutePath());
				classes.add(classObject);
				prefs.addProperty(LAST_ROSTER_PATH_KEY, rosterPath.getParent());
				

				FileUtil.writeString(PREF_FILE, prefs.toString());
			}
		});
		c.gridx = 2; c.gridy = componentList.size() + 1;
		panel.add(addButton, c);

		// Blank JPanel spacer
		c.gridx = 0; c.gridy = componentList.size() + 2;
		c.gridwidth = 5;
		panel.add(blank, c);

		// Take Attendance button
		attendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ComponentGroup selectedGroup = null;
				for (ComponentGroup g : componentList) {
					if (g.getCheckBox().isSelected()) {
						selectedGroup = g;
						break;
					}
				}
				
				// Holds output to display later in a scrollable JFrame
				String consoleOutput = "";
				
				PrintStream debug = debugState ? new PrintStream(System.out) : new PrintStream(new OutputStream() { public void write(int b) {	}});
				debug.println("DEBUG output is turn ON");
								
				//  1. Read Roster names into a List of Strings
				File rosterFile = new File(selectedGroup.getRosterPath());

				debug.println("Roster chosen: " + rosterFile.getName() + "\n");
				
				try (Scanner in = new Scanner(rosterFile)) {
					ArrayList<String> rosterList = new ArrayList<String>();
					
					while (in.hasNextLine()) {
						rosterList.add(in.nextLine());
					}


					//	2. Initialize a new List of ChatUser
					ArrayList<ChatUser> chatUserList = new ArrayList<ChatUser>();

					//	Open Zoom chat file
					JFileChooser chatChooser = new JFileChooser();
					chatChooser.setDialogTitle("Select a chat session");
					if (prefs.has(LAST_CHAT_PATH_KEY)) chatChooser.setCurrentDirectory(new File(prefs.get(LAST_CHAT_PATH_KEY).getAsString()));
					chatChooser.showOpenDialog(null);
					File chatFile = chatChooser.getSelectedFile();
					if (chatFile == null)
						return;

					// Update prefs file to point to this folder
					debug.println("Updating prefs to: " + chatFile.getAbsolutePath());
					prefs.addProperty(LAST_CHAT_PATH_KEY, chatFile.getAbsolutePath());
					
					FileUtil.writeString(PREF_FILE, prefs.toString());
						
					// Note:  Each line in a Zoom chat file ends in "\r\n" but individual
					//        chat messages may contain separate "\r" or "\n" separately.
					
					try (Scanner cfin = new Scanner(chatFile, "UTF-8")){
//						in = new Scanner(chatFile);
						cfin.useDelimiter("\r\n");
					//  3. For each line of the Zoom Chat
						while (cfin.hasNext()) {
				        	
							String line = cfin.next();
							debug.println(line);
							if (line.trim().length() == 0) continue;
							// Read first token into a time String and trim it
							String time = line.substring(0, line.toLowerCase().indexOf("from")).trim();
							
							// Read rest of line and trim it
							line = line.substring(line.toLowerCase().indexOf("from") + "from".length()).trim();
							
							// We assume that the line is now one of two formats:
							//  - A message to "Everyone" has the format "<name> : <message>"
							//  - A private message has the format "<name>  to  <name>(Privately) : <message>
							// Note:  Sometimes the file has the format "<name> to <name>(Privately)...
							//        with only one space instead of two.  >:(
							
							// If the line contains " to " before ":" then it is format 2
							// else format 1
							boolean format1 = line.toLowerCase().contains(" to ") && line.toLowerCase().indexOf(" to ") < line.indexOf(":") ? false : true;
							String name, message;
							
							// Format 1: Extract <name> and <message>
							if (format1) {
								debug.println("Format 1");
								debug.println(line);
								name = line.substring(0, line.indexOf(":")).trim();
								message = line.substring(line.indexOf(":") + 1).trim();
							}
							// Format 2: Extract <name> and skip everything up to " : ".
							// Extract <message>
							else {
								debug.println("Format 2!!!!!!");
								debug.println(line);
								name = line.substring(0, line.toLowerCase().indexOf(" to ")).trim();
								message = line.substring(line.indexOf(":") + 1).trim();
							}

							debug.println("Time--" + time + "--");
							debug.println("Name--" + name + "--");
							debug.println("Msg --" + message + "--");

							// Search ChatUser list for <name>
							// If none exists, build new ChatUser object from the data and put in list
							int index = search(chatUserList, name);
							if (index < 0) {
								chatUserList.add(new ChatUser(name, time, time));
							}
							// Otherwise update the last entry time
							else {
								chatUserList.get(index).setLastEntry(time);
							}
						}
						
						cfin.close();
					} catch (FileNotFoundException err) {
						err.printStackTrace();
					}
					
					

					FileWriter outFile = null;
					
					try {
						String saveFileName = chatFile.getParent() + "\\attendance_out.csv";
						outFile = new FileWriter(new File(saveFileName));
					} catch (IOException err) {
						// TODO Auto-generated catch block
						err.printStackTrace();
					}

					ArrayList<String> absentList = new ArrayList<String>();

					// Number of present students listed on roster
					int presentTotal = 0;
					
					System.out.println("\nClass roster (" + rosterList.size() + ")\n");
					consoleOutput += "<h2>Class roster (" + rosterList.size() + ")</h2>";
					
					// 4. Finally, loop through the original list of roster names
					for (String name : rosterList) {
						// If current name is not found in ChatUser, this student was absent
						// so print out absent information
						int index = search(chatUserList, name);
						if (index < 0) {
							System.out.println(name + "*** Absent ***");
							consoleOutput += "<font color=\"#ff0000\">" + name + "*** Absent ***</font><br>";
							try {
								outFile.write(name + "," + "ABSENT\r\n");
								absentList.add(name);
							} catch (IOException err) {
								err.printStackTrace();
							}
						}
						// Otherwise print out the matching ChatUser object and remove this
						// record from the ChatUser list
						else {
							presentTotal++;
							System.out.println(name + ", " + chatUserList.get(index).getFirstEntry() + ", " + chatUserList.get(index).getLastEntry());
							consoleOutput += name + ", " + chatUserList.get(index).getFirstEntry() + ", " + chatUserList.get(index).getLastEntry() + "<br>";
							try {
								outFile.write(name + "," + chatUserList.get(index).getFirstEntry() + "," + chatUserList.get(index).getLastEntry() + "\r\n");
							} catch (IOException err) {
								err.printStackTrace();
							}
							chatUserList.remove(index);
						}
					}

//					System.out.println("\r\nRoster Total (" + rosterList.size() + ")");
//					System.out.println("Present (" + presentTotal + ")");
					System.out.println("\r\nAbsent (" + absentList.size() + ")");
//					consoleOutput += "<h2>Roster Total (" + rosterList.size() + ")</h2>";
//					consoleOutput += "<h2>Present (" + presentTotal + ")</h2>";
					consoleOutput += "<h2>Absent (" + absentList.size() + ")</h2>";
					try {
//						outFile.write("\r\nRoster Total (" + rosterList.size() + ")");
//						outFile.write("\r\nPresent (" + presentTotal + ")\r\n");
						outFile.write("\r\nAbsent (" + absentList.size() + ")\r\n");
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					for (String name : absentList) {
						System.out.println(name);
						consoleOutput += "<font color=\"#ff0000\">" + name + "</font><br>";
						try {
							outFile.write(name + "\r\n");
						} catch (IOException err) {
							err.printStackTrace();
						}
					}

					// Anybody left in the ChatUser list does not match with a roster name.
					// Print this list out
					System.out.println("\nNot on roster (" + chatUserList.size() + ")");
					consoleOutput += "<h2>Not on roster (" + chatUserList.size() + ")</h2>";
					try {
						outFile.write("\r\nNot on roster (" + chatUserList.size() + ")\r\n");
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					for (ChatUser user : chatUserList) {
						System.out.println(user.getName() + ", " + user.getFirstEntry() + ", " + user.getLastEntry());			
						consoleOutput += "<font color=\"#ff0000\">" + user.getName() + ", " + user.getFirstEntry() + ", " + user.getLastEntry() + "</font><br>";
						try {
							outFile.write(user.getName() + "," + user.getFirstEntry() + "," + user.getLastEntry()+"\r\n");
						} catch (IOException err) {
							err.printStackTrace();
						}
					}

					consoleOutput += "<br><br>";
					
					try {
						outFile.close();
						debug.close();
					} catch (IOException err) {
						err.printStackTrace();
					}
				} catch (FileNotFoundException err) {
					err.printStackTrace();
					System.out.println("Roster not found:");
					System.out.println(rosterFile.getAbsolutePath());
					consoleOutput += "<font color=\"#ff0000\">" + "Roster not found:<br>";
					consoleOutput += rosterFile.getAbsolutePath() + "</font><br>";
					return;
				}

				JFrame popup = new JFrame("Attendance Results");
				popup.setSize(500, 800);
//				JTextArea content = new JTextArea(consoleOutput);
				JEditorPane content = new JEditorPane("text/html", consoleOutput);
				JScrollPane scrollContent = new JScrollPane(content);
				popup.add(scrollContent);
				content.setCaretPosition(content.getDocument().getLength());								
				Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
				popup.setLocation(dim.width/2-popup.getSize().width/2, dim.height/2-popup.getSize().height/2);
				popup.setVisible(true);
			}
		});

		c.gridx = 0; c.gridy = componentList.size() + 3;
		c.gridwidth = 5;
		panel.add(attendButton, c);
		
		// ButtonGroup
		for (ComponentGroup g : componentList) {
			bg.add(g.getCheckBox());
		}

		win.getContentPane().add(panel);
		
		win.pack();
		win.setVisible(true);
	}
	
	protected static int componentIndex(ArrayList<ComponentGroup> componentList, Object source) {
		for (int i = 0; i < componentList.size(); i++) {
			if (componentList.get(i).getEditButton().equals(source) || componentList.get(i).getDeleteButton().equals(source))
				return i;
		}
		return -1;
	}

	public static int search(ArrayList<ChatUser> list, String name) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getName().equalsIgnoreCase(name))
				return i;
		}
		return -1;
	}

	private static JsonObject getPrefs() throws JsonSyntaxException, FileNotFoundException, IOException {
		File prefFile = new File(PREF_FILE);
		if(prefFile.exists() && !prefFile.isDirectory()) { 
			return FileUtil.getObj(PREF_FILE).getAsJsonObject();
		}
		return new JsonObject(); // no prefs file
	}
}

